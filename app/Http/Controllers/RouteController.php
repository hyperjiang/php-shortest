<?php

namespace App\Http\Controllers;

use App\Validators\LocationValidator;
use Illuminate\Http\Request;

class RouteController extends Controller
{
    protected function error($msg)
    {
        return response()->json(['error' => $msg]);
    }

    // POST /route: Submit start point and drop-off locations
    public function post(Request $request)
    {
        try {
            $content = $request->getContent();
            app('log')->debug('req: ' . $content);
            $newContent = LocationValidator::validate($content);
            app('log')->debug('filtered: ' . $newContent);
            $token = app('shortest-path-service')->save($newContent);
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }

        return response()->json(['token' => $token]);
    }

    // GET /route/<TOKEN>: Get shortest driving route
    public function get($token)
    {
        $res = app('shortest-path-service')->get($token);
        return response()->json($res);
    }
}
