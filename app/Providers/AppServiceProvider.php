<?php

namespace App\Providers;

use App\Services\GoogleMapService;
use App\Services\ShortestPathService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('shortest-path-service', function ($app) {
            return new ShortestPathService;
        });

        $this->app->singleton('google-map-service', function ($app) {
            return new GoogleMapService;
        });
    }
}
