<?php

namespace App\Validators;

use App\Exceptions\ValidationException;

/**
 * Validator for location inputs
 */
class LocationValidator
{
    /**
     * Remove duplicated element in the coordinates array
     *
     * @param  array   $coordinates
     * @return array
     */
    public static function removeDuplicated(array $coordinates)
    {
        $arr = [];
        foreach ($coordinates as $row) {
            $arr[$row[0] . '-' . $row[1]] = 1;
        }

        $result = [];

        foreach (array_keys($arr) as $row) {
            $result[] = explode('-', $row);
        }

        return $result;
    }

    /**
     * Run the validator and return the filtered input
     *
     * @throws App\Exceptions\ValidationException
     * @return string
     */
    public static function validate(string $content)
    {
        $json = json_decode($content, true);

        if (empty($json) || !is_array($json)) {
            throw new ValidationException('Invalid input format', 400);
        }

        foreach ($json as $row) {
            if (!is_array($row) || count($row) != 2 || !is_numeric($row[0]) || !is_numeric($row[1])) {
                throw new ValidationException('Invalid input format', 400);
            }
        }

        // remove duplicated coordinates
        $arr = self::removeDuplicated($json);

        $count = count($arr);
        if ($count < 2 || $count > 10) {
            throw new ValidationException(
                'The number of locations should be between 2 and 10',
                400
            );
        }

        return json_encode($arr);
    }
}
