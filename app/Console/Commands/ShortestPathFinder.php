<?php

namespace App\Console\Commands;

use App\Models\Route;
use Illuminate\Console\Command;

class ShortestPathFinder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'finder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Shortest path finder';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = Route::where('status', Route::STATUS_NEW)->get();

        if ($data->isEmpty()) {
            app('log')->warn('No data to process');
            $this->warn('No data to process');
            die;
        }

        foreach ($data as $row) {
            $this->info($row->token);
            app('log')->info($row->token);
            app('shortest-path-service')->process($row);
        }
    }
}
