<?php

namespace App\Services;

use drupol\phpermutations\Generators\Permutations;

// Mathematical model of calculating the shortest route
class MathService
{
    /**
     * Coordinates [lat, lon] array
     * The first one is the starting point
     *
     * @var array
     */
    protected $locations = [];

    /**
     * The distance and duration matrix
     * structure: [origin index][destination index] => [distance, duration]
     *
     * @var array
     */
    protected $matrix = [];

    /**
     * Store the total distance result
     *
     * @var integer
     */
    protected $total_distance = 0;

    /**
     * Store the total time result
     *
     * @var integer
     */
    protected $total_time = 0;

    /**
     * Store the shortest route
     *
     * @var array
     */
    protected $shortest_route = [];

    public function __construct($locations = [], $matrix = [])
    {
        $this->setLocations($locations);
        $this->setMatrix($matrix);
    }

    public function getLocations()
    {
        return $this->locations;
    }

    public function getMatrix()
    {
        return $this->matrix;
    }

    public function getTotalDistance()
    {
        if ($this->total_distance == 0) {
            $this->run();
        }
        return $this->total_distance;
    }

    public function getTotalTime()
    {
        if ($this->total_time == 0) {
            $this->run();
        }
        return $this->total_time;
    }

    public function getShortestRoute()
    {
        if (empty($this->shortest_route)) {
            $this->run();
        }
        return $this->shortest_route;
    }

    public function getCoordinatesOfShortestRoute()
    {
        $route  = $this->getShortestRoute();
        $result = [];
        foreach ($route as $r) {
            $result[] = $this->locations[$r];
        }
        return $result;
    }

    public function setLocations($locations)
    {
        $this->locations = $locations;
        return $this;
    }

    public function setMatrix($matrix)
    {
        $this->matrix = $matrix;
        return $this;
    }

    /**
     * Run and store the results
     *
     * @return MathService
     */
    public function run()
    {
        $this->total_distance = $this->total_time = PHP_INT_MAX;
        $this->shortest_route = [];

        $keys         = array_keys($this->getLocations());
        $origin       = array_shift($keys);
        $permutations = new Permutations($keys);

        foreach ($permutations->generator() as $route) {

            // add the origin back to the route
            array_unshift($route, $origin);

            $result = $this->calculate($route);

            if (empty($this->shortest_route) || $this->total_distance > $result[0]) {
                $this->shortest_route = $route;
                $this->total_distance = $result[0];
                $this->total_time     = $result[1];
            }
        }
        return $this;
    }

    /**
     * Calculate the distance and duration of the given route
     *
     * @param  array  $route
     * @return void
     */
    protected function calculate($route)
    {
        $distance = $duration = 0;
        $matrix   = $this->getMatrix();
        for ($i = 0; $i < count($route) - 1; $i++) {
            if (!isset($matrix[$route[$i]]) ||
                !isset($matrix[$route[$i]][$route[$i + 1]]) ||
                !isset($matrix[$route[$i]][$route[$i + 1]][0]) ||
                !isset($matrix[$route[$i]][$route[$i + 1]][1]) ||
                $matrix[$route[$i]][$route[$i + 1]][0] == -1 ||
                $matrix[$route[$i]][$route[$i + 1]][1] == -1
            ) { // dead route
                return [PHP_INT_MAX, PHP_INT_MAX];
            }
            $distance += $matrix[$route[$i]][$route[$i + 1]][0];
            $duration += $matrix[$route[$i]][$route[$i + 1]][1];
        }
        return [$distance, $duration];
    }
}
