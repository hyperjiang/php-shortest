<?php

namespace App\Services;

use App\Models\Route;
use Ramsey\Uuid\Uuid;

class ShortestPathService
{
    /**
     * Get a random string
     *
     * @param  integer  $length
     * @return string
     */
    protected function getRandomString($length = 8)
    {
        if (function_exists('openssl_random_pseudo_bytes')) {
            return bin2hex(openssl_random_pseudo_bytes($length));
        }
        if (function_exists('mcrypt_create_iv')) {
            return bin2hex(mcrypt_create_iv($length, MCRYPT_DEV_URANDOM));
        }
        return uniqid();
    }

    /**
     * Save the input locations and return the token
     *
     * @param  string   $input
     * @return string
     */
    public function save($input)
    {
        try {
            $token = Uuid::uuid1()->toString();
        } catch (\Exception $e) {
            // if we got a "Could not gather sufficient random data" Exception
            // then switch to use uuid3
            $token = Uuid::uuid3(Uuid::NAMESPACE_DNS, $this->getRandomString());
        }

        $model         = new Route;
        $model->token  = $token;
        $model->input  = $input;
        $model->status = Route::STATUS_NEW;
        $model->save();

        return $token;
    }

    /**
     * Get the shortest path result
     *
     * @param  string  $token
     * @return array
     */
    public function get($token)
    {
        $model = Route::where('token', $token)->first();
        if (!$model) {
            return $this->fail('Invalid token');
        }

        if ($model->status == Route::STATUS_NEW) {
            return ['status' => 'in progress'];
        } elseif ($model->status == Route::STATUS_FAILURE) {
            return $this->fail($model->error);
        }

        return [
            'status'         => 'success',
            'path'           => json_decode($model->path, true),
            'total_distance' => $model->total_distance,
            'total_time'     => $model->total_time,
        ];
    }

    /**
     * Find the shortest path for the unprocessed inputs and change their status in DB
     *
     * @param  Route   $model
     * @return Route
     */
    public function process(Route $model)
    {
        try {
            $locations = json_decode($model->input, true);
            $result    = app('google-map-service')->getShortestPath($locations);

            $model->path           = $result['path'];
            $model->total_distance = $result['total_distance'];
            $model->total_time     = $result['total_time'];
            $model->status         = Route::STATUS_SUCCESS;
        } catch (\Exception $e) {
            $model->status = Route::STATUS_FAILURE;
            $model->error  = $e->getMessage();
        }

        $model->save();
        return $model;
    }

    protected function fail($msg)
    {
        return ['status' => 'failure', 'error' => $msg];
    }
}
