<?php

namespace App\Services;

use App\Exceptions\GoogleMapApiException;
use Http\Adapter\Guzzle6\Client;
use Http\Message\MessageFactory\GuzzleMessageFactory;
use Ivory\GoogleMap\Base\Coordinate;
use Ivory\GoogleMap\Service\Base\Location\CoordinateLocation;
use Ivory\GoogleMap\Service\DistanceMatrix\DistanceMatrixService;
use Ivory\GoogleMap\Service\DistanceMatrix\Request\DistanceMatrixRequest;

class GoogleMapService
{
    protected $distance_matrix_service = null;

    public function __construct()
    {
        $this->distance_matrix_service = new DistanceMatrixService(new Client(), new GuzzleMessageFactory());
    }

    public function getDistanceMatrixService()
    {
        return $this->distance_matrix_service;
    }

    /**
     * Use Google Maps Distance Matrix API to get the distance and duration matrix
     *
     * @param  array $locations
     * @return array structure: [origin index][destination index] => [distance, duration]
     */
    public function getMatrix(array $locations)
    {
        $origins = array_map(function ($item) {
            return new CoordinateLocation(new Coordinate($item[0], $item[1]));
        }, $locations);

        $destinations = array_slice($origins, 1);

        $response = $this->getDistanceMatrixService()->process(new DistanceMatrixRequest(
            $origins,
            $destinations
        ));

        if ($response->getStatus() != 'OK') {
            throw new GoogleMapApiException('Fail to fetch matrix');
        }

        $matrix = [];
        foreach ($response->getRows() as $i => $row) {
            foreach ($row->getElements() as $j => $element) {
                if ($element->getStatus() != 'OK') { // dead route
                    $matrix[$i][$j + 1] = [-1, -1];
                } else {
                    $matrix[$i][$j + 1] = [
                        $element->getDistance()->getValue(),
                        $element->getDuration()->getValue(),
                    ];
                }
            }
        }

        return $matrix;
    }

    /**
     * Get the shortest path
     *
     * @param  array   $locations
     * @return array
     */
    public function getShortestPath(array $locations)
    {
        $math = new MathService($locations, $this->getMatrix($locations));

        return [
            'path'           => json_encode($math->getCoordinatesOfShortestRoute()),
            'total_distance' => $math->getTotalDistance(),
            'total_time'     => $math->getTotalTime(),
        ];
    }
}
