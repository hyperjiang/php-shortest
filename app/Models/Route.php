<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    const STATUS_NEW     = 0;
    const STATUS_SUCCESS = 1;
    const STATUS_FAILURE = 2;

    protected $guarded = [];
}
