<?php

use App\Exceptions\ValidationException;
use App\Validators\LocationValidator;

class LocationValidatorTest extends TestCase
{
    protected $validator;

    public function setup()
    {
        $this->validator = new LocationValidator();
    }

    /**
     * @dataProvider dataProvider
     */
    public function testRemoveDuplicated($input, $expected)
    {
        $result = $this->validator->removeDuplicated($input);
        $this->assertEquals($expected, $result);
    }

    /**
     * @dataProvider validDataProvider
     */
    public function testValid($input, $expected)
    {
        $this->assertEquals($expected, $this->validator->validate($input));
    }

    /**
     * @dataProvider invalidDataProvider
     */
    public function testInvalid($input)
    {
        $this->expectException(ValidationException::class);
        $this->validator->validate($input);
    }

    /**
     * Data provider for testRemoveDuplicated
     *
     * @return void
     */
    public function dataProvider()
    {
        return [
            [
                [
                    [1, 1],
                    [2, 2],
                ],
                [
                    [1, 1],
                    [2, 2],
                ],
            ],
            [
                [
                    [1, 1],
                    [2, 2],
                    [1, 1],
                    [2, 2],
                ],
                [
                    [1, 1],
                    [2, 2],
                ],
            ],
            [
                [
                    [1, 1],
                    [2, 2],
                    [1, 1],
                    [1, 2],
                ],
                [
                    [1, 1],
                    [2, 2],
                    [1, 2],
                ],
            ],
        ];
    }

    /**
     * Valid data provider for testValid
     */
    public function validDataProvider()
    {
        return [
            [
                '[
                    ["22.372081", "114.107877"],
                    ["22.284419", "114.159510"],
                    ["22.326442", "114.167811"]
                ]',
                '[["22.372081","114.107877"],["22.284419","114.159510"],["22.326442","114.167811"]]',
            ],
            [
                '[
                    ["1", "1"],
                    ["2", "2"]
                ]',
                '[["1","1"],["2","2"]]',
            ],
            [
                '[
                    ["1", "1"],
                    ["2", "2"],
                    ["1", "1"],
                    ["2", "2"]
                ]',
                '[["1","1"],["2","2"]]',
            ],
        ];
    }

    /**
     * Invalid data provider for testInvalid
     */
    public function invalidDataProvider()
    {
        return [
            [
                '[
                    ["1", "1", "1"],
                    ["2", "2"],
                    ["3", "3"]
                ]',
            ],
            [
                '[["1", "1"]]',
            ],
            [
                '[]',
            ],
            [
                'xxx',
            ],
            [
                '[
                    ["1", "1"],
                    ["1", "1"],
                    ["1", "1"]
                ]',
            ],
            [
                '[
                    ["1"],
                    ["2", "2"],
                    ["3", "3"]
                ]',
            ],
            [
                '[
                    ["1",,],
                    ["2", "2"],
                    ["3", "3"]
                ]',
            ],
            [
                '[
                    ["1", "1"],
                    ["2", "a"],
                    ["3", "3"]
                ]',
            ],
            [
                '[
                    ["1", "1"],
                    ["2", "2"],
                    ["3", "3"],
                    ["4", "4"],
                    ["5", "5"],
                    ["6", "6"],
                    ["7", "7"],
                    ["8", "8"],
                    ["9", "9"],
                    ["10", "10"],
                    ["11", "11"],
                ]',
            ],
        ];
    }
}
