<?php

use App\Services\MathService;

class MathServiceTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     */
    public function testRun(array $locations, array $matrix, int $expected)
    {
        $service = new MathService($locations, $matrix);
        $service->run();
        $this->assertEquals($expected, $service->getTotalDistance());
    }

    public function dataProvider(): array
    {
        $maxLocations = [];
        $maxMatrix    = [];
        for ($i = 0; $i < 10; $i++) {
            $maxLocations[] = [$i, $i];
            for ($j = 1; $j < 10; $j++) {
                $maxMatrix[$i][$j] = [$j, $j];
            }
        }

        return [
            [
                [
                    ["0", "0"],
                    ["1", "1"],
                    ["2", "2"],
                ],
                [
                    0 => [
                        1 => [1, 1],
                        2 => [2, 2],
                    ],
                    1 => [
                        2 => [3, 3],
                    ],
                    2 => [
                        1 => [1, 1],
                    ],
                ],
                3,
            ],
            [
                [
                    ["0", "0"],
                    ["1", "1"],
                    ["2", "2"],
                    ["3", "3"],
                ],
                [
                    0 => [
                        1 => [1, 1],
                        2 => [2, 2],
                        3 => [1, 1],
                    ],
                    1 => [
                        2 => [3, 3],
                        3 => [2, 2],
                    ],
                    2 => [
                        1 => [1, 1],
                        3 => [4, 4],
                    ],
                    3 => [
                        1 => [3, 3],
                        2 => [3, 3],
                    ],
                ],
                5,
            ],
            [
                [
                    ["0", "0"],
                    ["1", "1"],
                    ["2", "2"],
                    ["3", "3"],
                ],
                [
                    0 => [
                        1 => [3, 3],
                        2 => [3, 3],
                        3 => [3, 3],
                    ],
                    1 => [
                        0 => [1, 1],
                        2 => [1, 1],
                        3 => [1, 1],
                    ],
                    2 => [
                        0 => [1, 1],
                        1 => [2, 2],
                        3 => [2, 2],
                    ],
                    3 => [
                        0 => [1, 1],
                        1 => [3, 3],
                        2 => [3, 3],
                    ],
                ],
                6,
            ],
            [
                $maxLocations,
                $maxMatrix,
                45,
            ],
        ];
    }
}
