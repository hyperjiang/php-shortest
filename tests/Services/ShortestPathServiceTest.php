<?php

use App\Models\Route;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ShortestPathServiceTest extends TestCase
{
    // reset database after each test
    use DatabaseTransactions;

    public function testAllMethods()
    {
        $input = [
            ["22.372081", "114.107877"],
            ["22.284419", "114.159510"],
        ];

        $token = app('shortest-path-service')->save(json_encode($input));

        $res = app('shortest-path-service')->get($token);

        $this->assertEquals(['status' => 'in progress'], $res);

        $model        = Route::where('token', $token)->first();
        $updatedModel = app('shortest-path-service')->process($model);

        $this->assertEquals($updatedModel->total_distance, 15534);
    }

}
