<?php

use App\Services\GoogleMapService;

class GoogleMapServiceTest extends TestCase
{
    protected $service;

    public function setup()
    {
        $this->service = new GoogleMapService();
    }

    /**
     * @dataProvider dataProvider
     */
    public function testGetShortestPath(array $locations, int $expected)
    {
        $result = $this->service->getShortestPath($locations);
        $this->assertEquals($expected, $result['total_distance']);
    }

    public function dataProvider(): array
    {
        return [
            [
                [
                    ["22.372081", "114.107877"],
                    ["22.284419", "114.159510"],
                    ["22.326442", "114.167811"],
                ],
                18153,
            ],
            // note that this array contains duplicated locations
            [
                [
                    ["22.372081", "114.107877"],
                    ["22.284419", "114.159510"],
                    ["22.326442", "114.167811"],
                    ["22.284419", "114.159510"],
                    ["22.326442", "114.167811"],
                    ["22.284419", "114.159510"],
                    ["22.326442", "114.167811"],
                    ["22.284419", "114.159510"],
                    ["22.326442", "114.167811"],
                    ["22.284419", "114.159510"],
                ],
                18153,
            ],
        ];
    }
}
