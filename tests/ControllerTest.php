<?php

use App\Models\Route;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ControllerTest extends TestCase
{
    // reset database after each test
    use DatabaseTransactions;

    /**
     * Test the website index page.
     *
     * @return void
     */
    public function testIndex()
    {
        $this->get('/');

        $this->assertEquals(
            $this->app->version(), $this->response->getContent()
        );
    }

    /**
     * @dataProvider dataProvider
     */
    public function testPostRoute(array $input, array $expected)
    {
        $this->json('POST', '/route', $input)
            ->seeJsonStructure($expected);
    }

    public function testGetRoute()
    {
        $token = 'xxx';
        $this->json('GET', '/route/' . $token)
            ->seeJsonEquals([
                "status" => "failure",
                "error"  => "Invalid token",
            ]);

        $input = [
            ["22.372081", "114.107877"],
            ["22.284419", "114.159510"],
        ];
        $this->json('POST', '/route', $input);
        $content = $this->response->getContent();
        $arr     = json_decode($content, true);
        $token   = $arr['token'];
        $this->json('GET', '/route/' . $token)
            ->seeJson([
                'status' => 'in progress',
            ]);

        $model = Route::where('status', Route::STATUS_SUCCESS)->first();
        if ($model) {
            $this->json('GET', '/route/' . $model->token)
                ->seeJsonEquals([
                    'status'         => 'success',
                    'path'           => $model->path,
                    'total_distance' => $model->total_distance,
                    'total_time'     => $model->total_time,
                ]);
        }
    }

    public function dataProvider(): array
    {
        return [
            [
                [
                    ["22.372081", "114.107877"],
                    ["22.284419", "114.159510"],
                    ["22.326442", "114.167811"],
                ],
                ['token'],
            ],
            [
                [
                    ["22.372081", "114.107877"],
                ],
                ['error'],
            ],
        ];
    }
}
