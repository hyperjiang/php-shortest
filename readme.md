# Shortest Path

Demos of calculating shortest driving path by using Google Maps API

## Frameworks

This project is based on [Lumen PHP framework](https://github.com/laravel/lumen)

## Asynchronous

This project use the task scheduling of Lumen to process the input locations, that means the shortest path finder is running asynchronous

## Algorithm

This is actually a [Hamiltonian path problem](https://en.wikipedia.org/wiki/Hamiltonian_path), if the number of destinations is big, it will cost a large amount of calculation time when we traverse all the possible routes, so this project has a maximum of 10 locations input limitation per request.

We use [Google Maps Distance Matrix API](https://developers.google.com/maps/documentation/distance-matrix/) to calculate the distance and duration which has a limitation of maximum of 25 origins or 25 destinations or 100 elements per request, our limitation of 10 locations input per request can meet its requirement.

We can't trust user input, so we will validate the input and also remove the duplicated coordinates. e.g. if the input locations are `[[lat1, lon1], [lat2, lon2], [lat1, lon1]]`, it will be filtered into `[[lat1, lon1], [lat2, lon2]]`

The processing procedure is, first we pick up one route and walk though all the locations and record the total distance and time, then we try another route and if the total distance is greater than the previous one  we will stop and switch to another route, otherwise if the total distance is shorter then we will select it as the current shortest route, and so on until we traverse all the routes.

## Installation

```
# initialize the containers
docker-compose up --build -d
```
Currently the containers will bind ports `10086` and `10010` to the localhost, and the lumen container will run `composer install` and `php artisan migrate` after launch, so you may need to wait a minute for the initialization.

*If you fail to mount the current directory into container for some weird reason, you may need to initialize it by the following commands:*

```
docker cp . lumen:app
docker-compose exec lumen /app/scripts/init.sh
```

## Testing

**Submit locations**

```
curl -X POST http://localhost:10086/route -d '[["22.372081", "114.107877"],["22.284419", "114.159510"],["22.326442", "114.167811"]]'
```

**Get the shortest path result**

```
curl http://localhost:10086/route/{token}
```

**Unit test**

```
docker-compose exec lumen composer test
```


