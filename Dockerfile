FROM richarvey/nginx-php-fpm:php71
COPY ./scripts /var/www/html/scripts
WORKDIR /app
ENV COMPOSER_ALLOW_SUPERUSER 1
ENV RUN_SCRIPTS 1
RUN sed -i -e 's/try_files $uri $uri\/ =404/try_files $uri $uri\/ \/index.php$is_args$args/g' /etc/nginx/sites-enabled/default.conf
RUN sed -i -e 's/root \/var\/www\/html;/root \/app\/public;/g' /etc/nginx/sites-enabled/default.conf
RUN echo "* * * * * /app/scripts/cron.sh >> /dev/null 2>&1" >> /etc/crontabs/root
